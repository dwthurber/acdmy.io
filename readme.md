# Acdmy.io

## (re)Designing the Classroom for the 21st Century

Acdmy.io is the (re)designed classroom for the 21st century. Although it is envisioned as a way for a group of people to meet online and communicate/collaborate in real time using video, audio, text, etc., Acdmy.io is built to be different.

It is not a web conferencing platform. It is an online learning environment conducive to student interaction in both formal and informal ways. It is based on concepts grounded in educational theory and the learning science to promote social and emotional presence, practical inquiry, motivation, and active learning.
